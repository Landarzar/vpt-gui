{-# LANGUAGE ExistentialQuantification, RankNTypes #-}

{-| Automaten Typklassen
 - Author: Kai Sauerwald
 - Die Arbeit kann gerne unter Namensnennung verändert und weiter gegeben werden.
 -}
module Modell.Automata where

import Data.Maybe

data TransitionTyp q w s = RegularTransition (q,w,q) | ParametricTransition (q,w,s,q)

{- | 
 - Diese Typklasse ist stellt ein Abstraction eines AutomatenModell da. 
 - Mdl ist ein Typ 3. Ordnung mit dem Typparametern:
 - q - Der Zustandstyp
 - w - Der Typ des Alphabet
 - s - Parametertyp (Simuliert Stack oder Bandpositionen einer TM)
 -}
class AutomataModell mdl where
    -- ^ Die Zustände des Automaten
    states      :: mdl q w s   -- ^ Der Automat
                -> [q]         -- ^ Returnt die Liste an Zuständen

    -- ^ Die Startzustände des Automaten
    start       :: mdl q w s   -- ^ Der Automat
                -> q           -- ^ Returnt den Startzustand

    -- ^ Die akzeptierenden Zustande des Automanten
    final       :: mdl q w s   -- ^ Der Automat
                -> [q]         -- ^ Returnt die Liste an akzeptierenden Zuständen
            
    -- ^ Die Übergänge des Automaten
    transitions :: mdl q w s   -- ^ Der Automat
                -> [(q,w,s,q)] -- ^ Liste der Transitionen

    -- ^ Der Fehlerzustand bei eingabe-/übergangs- Fehlern
    failure     :: mdl q w s   -- ^ Der Automat
                -> q           -- ^ Der Fehlerzustand des Automaten
            
    -- ^ Gibt eine Startinstanz des Automaten zurück
    initial     :: (Automata aut) 
                => mdl q w s
                -> aut p z s
            
{- |
  - Diese Typklasse ist für Automatenrepräsentationen
  -}
class (AutomataModell aut) => Automata aut where
    -- ^ Der Zustanden in dem sich der Automat befindet.
    state   :: aut q w s   -- ^ Der Automat
            -> Maybe (q,s)     -- ^ Der Zustand
            
    -- ^ Ist der Automat in einem Finalen Zustand
    isFinal :: (Eq q)
            => aut q w s   -- ^ der Automat
            -> Bool        -- ^ True wenn Final, False anderernfalls
    isFinal aut = if isJust $ state aut then ((fst . fromJust) $ state aut) `elem` final aut else False 

    -- ^ Stellt fest ob es einen akzeptienden Lauf für das Wort w gibt
    accept  :: (Eq q) 
            => aut q w s   -- ^ Der Automat
            -> [w]         -- ^ Das Eingabewort w
            -> Bool        -- ^ Returnt True wenn es einen akzeptierenden Lauf gibt für das Wort w von den Startzuständen aus.
    accept aut wx = isFinal $ stepE aut wx

    -- ^ Die fortführung von 'step' auf Wörter
    stepE   :: aut q w s   -- ^ Der Automat
            -> [w]         -- ^ Das Eingabewort w
            -> aut q w s   -- ^ Der erreichte Automat
    stepE = foldl step

    -- ^ Führt eine Zustandsüberführung aus.
    step    :: aut q w s   -- ^ Der Automat
            -> w           -- ^ Das Eingabesymbol
            -> aut q w s   -- ^ Der erreichte Zustand
    
    -- ^ Führt einen \"run\" aus, gibt alle zustqnde zurück.
    run     :: aut q w s   -- ^ Der Automat
            -> [w]         -- ^ Das Eingabewort
            -> [aut q w s] -- ^ Der \"run\"
    run aut []     = [aut]
    run aut (w:wx) = aut : run (step aut w) wx
   

{-| 
 - Diese Typeklasse fasst die Eigenschaften von Transducern einmal zusammen.
 - Für eine Implementierung ist 'runAndTransduce' optional.
 -}
class Transducer tds where
    transduce       :: Automata (tds o)         -- Der Automat wird erzwungen
                    => (tds o) q w s            -- ^ Der Automat
                    -> [w]                      -- ^ Eingabewort
                    -> [o]                      -- ^ Ausgabewort
    transduce _  []      = []
    transduce tds (w:wx) = out ++ transduce naut wx
          where (naut,out) = transStep tds w

    runAndTransduce :: Automata (tds o)         -- Der Automat wird erzwungen
                    => tds o q w s              -- ^ Der Automat
                    -> [w]                      -- ^ Eingabewort
                    -> ([tds o q w s], [o])     -- ^ Ausgabe-Zustand und Wort
    runAndTransduce aut w = (run aut w,transduce aut w)

    transStep       :: Automata (tds o)         -- Der Automat wird erzwungen
                    => tds o q w s              -- ^ Der Automat
                    -> w                        -- ^ Eingabesymbol
                    -> (tds o q w s,[o])        -- ^ Ausgabe-Zustand und Symbol
