{-# LANGUAGE FlexibleInstances #-}
import Graphics.UI.Gtk
import Control.Concurrent
import Control.Arrow
import Control.Concurrent.STM
import Control.Monad
import System.Exit
import Graphics.UI.Gtk
import Graphics.Rendering.Cairo
import Graphics.UI.Gtk.Builder
import Graphics.UI.Gtk.Gdk.EventM (Modifier(..))
import System.Exit
import GUI.Drawing
import GUI.Editierbar
--import Modell.VPT
        
data TestGraph = TestGraph { nodes  :: [(Int, (Double,Double))],
                             edges :: [(Int,Int)],
                             labels :: [(Int,String)] }
                             
type Edu = String

instance Editierbar Edu where
      onEdit = Just

instance GraphDrawable TestGraph where
        getNodePos   = nodes
        getEdges     = edges
        getNodeLabel graph i = foldl (\s (j,n) -> if i == j then n else s ) (""::Edu) (labels graph)
        
instance GraphEventManager TestGraph where
        onNodeSelect g i = return g
        onNodeEdit   g i = return g
        onNodeCreate g (i,j) = do
                                 oNodes  <- return $ getNodePos g
                                 oEdges  <- return $ getEdges g
                                 mymax   <- return $ foldl (\i (n,_)-> max i n) 0 oNodes
                                 return (TestGraph (oNodes ++ [(mymax+1,(i,j))]) oEdges (labels g ++ [(mymax+1,"")]))
        onNodeDelete g i = return g
        onNodeMove   graph i p = do 
                               nNodes <- return $ map (\(node,pos) -> if node == i then (node,p) else (node,pos)) (nodes graph)
                               return (TestGraph nNodes (edges graph) (labels graph) )
        onEdgeCreate g i j = do
                                nEdges <- return $ if elem (i,j) (edges g) then edges g else (i,j):edges g
                                return (TestGraph (nodes g) nEdges (labels g) )
        onEdgeEdit   g i = return g
        onEdgeDelete g i = return g
        onEdgeSelect g i = return g
        onGraphDraw  g = return ()

main :: IO ()
main = do
  tGraph     <- newTVarIO (TestGraph [(0,(300,300)),(1,(300,100)),(2,(300,500)),(3,(100,300)),(4,(500,300)),(5,(500,500)),(6,(100,100)),(7,(500,100)),(8,(100,500)),(9,(700,350))] [(0,i)|i <-[1..9]] [])
  tSelected  <- newTVarIO (Nothing :: Maybe (Either Int (Int,Int)))
  tDragMode  <- newTVarIO DragNone 
  tPoint     <- newTVarIO (Nothing :: Maybe (Double,Double))
  
  initGUI
  exit        <- newEmptyMVar
  
--window <- windowNew
--drawingArea <- createDrawingArea tGraph tSelected tDragMode tPoint (\_ -> widgetDestroy window)
  
  gui <- builderNew
  builderAddFromFile gui "../window.glade"
  
  window <- builderGetObject gui castToWindow "window"
  drawingArea <- builderGetObject gui castToDrawingArea "mainDrawinArea"
  
  _ <- setupDrawingArea drawingArea tGraph tSelected tDragMode tPoint (\_ -> widgetDestroy window)
--  
--  set window [ containerBorderWidth := 10,
--               windowDefaultHeight := 700, 
--               windowDefaultWidth := 1000, 
--               containerChild := drawingArea ]
  window `onDestroy` onWindowDestroy exit
  
    
  timer <- forkIO $ do
      let printTime t = do {threadDelay 100000; postGUIAsync $ widgetQueueDraw drawingArea; printTime (mod (t+1) 1001)}
      printTime (0::Int)

  widgetShowAll window

  mainGUI
  killThread timer
  signal <- takeMVar exit
  exitWith signal
  
  
onWindowDestroy :: MVar ExitCode -> IO ()
onWindowDestroy exit = do { 
        print "END"; 
        putMVar exit ExitSuccess; mainQuit } 
    



