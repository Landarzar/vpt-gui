{-# LANGUAGE FlexibleInstances, InstanceSigs #-}
module GUI.GUIVPT where

import Modell.Automata
import GUI.Drawing
import Graphics.Rendering.Cairo

-- ! Dieser Typ steht für. 
-- q für den Typ der States
-- w für das Eingabealphabet
data AutomataModell mdl => GUIAutomata mdl q w = GUIAutomata { gModell :: mdl q w, gNodePos :: [(q,(Double,Double))], gFree :: [q] }

instance AutomataModell mdl => GraphDrawable (GUIIntAutomata mdl) where
        getNodePos   :: (GUIIntAutomata mdl) -> [(Int, (Double,Double))]
        getNodePos   = gNodePos
        getEdges     :: (GUIIntAutomata mdl) -> [(Int,Int)]
        getEdges _   = []
        getNodeLabel :: (GUIIntAutomata mdl) -> Int -> String
        getNodeLabel _ _ = "muh!" 
        
type GUIIntAutomata mdl = GUIAutomata mdl Int Char
        
instance AutomataModell mdl => GraphEventManager (GUIIntAutomata mdl) where
        onNodeSelect :: GUIIntAutomata mdl -> Int -> IO (GUIIntAutomata mdl)
        onNodeSelect g _ = return g
        -- Füge ein neuen Knoten hinzu
        onNodeCreate :: GUIIntAutomata mdl -> (Double,Double) -> IO (GUIIntAutomata mdl)
        onNodeCreate mdl (x,y) = do 
                                    return mdl
                                 where 
                                      aut = gModell mdl
        onNodeDelete :: GUIIntAutomata mdl -> Int -> IO (GUIIntAutomata mdl)
        onNodeEdit   :: GUIIntAutomata mdl -> Int -> IO (GUIIntAutomata mdl)
        onNodeEdit g _ = return g
        onNodeMove   :: GUIIntAutomata mdl -> Int -> (Double,Double) -> IO (GUIIntAutomata mdl)
        onEdgeCreate :: GUIIntAutomata mdl -> Int -> Int -> IO (GUIIntAutomata mdl)
        onEdgeEdit   :: GUIIntAutomata mdl -> Int -> IO (GUIIntAutomata mdl)
        onEdgeDelete :: GUIIntAutomata mdl -> (Int,Int) -> IO (GUIIntAutomata mdl)
        onEdgeSelect :: GUIIntAutomata mdl -> (Int,Int) -> IO (GUIIntAutomata mdl)
        onGraphDraw  :: GUIIntAutomata mdl -> Render ()
