module GUI.Drawing 
        (createDrawingArea, 
        GraphEventManager (..), 
        GraphDrawable(..),
        setupDrawingArea,
        incircle,
        DragMode (..),
        nodeRadius)  
        where
        
import Control.Monad (when)
import Graphics.UI.Gtk
import Graphics.Rendering.Cairo
import GUI.Editierbar
import Control.Concurrent.STM

nodeRadius :: Double
nodeRadius = 35.0

data DragMode = DragNone | DragNode | DragEdge deriving (Eq,Show)

class GraphDrawable a where
        getNodePos   :: a -> [(Int, (Double,Double))]
        getEdges     :: a -> [(Int,Int)]
        getNodeLabel :: Editierbar edit => a -> Int -> edit
        
        
--  Die Typklasse GraphEventManager
class GraphEventManager a where
        onNodeSelect :: a -> Int -> IO a
        onNodeCreate :: a -> (Double,Double) -> IO a
        onNodeDelete :: a -> Int -> IO a
        onNodeEdit   :: a -> Int -> IO a
        onNodeMove   :: a -> Int -> (Double,Double) -> IO a
        onEdgeCreate :: a -> Int -> Int -> IO a
        onEdgeEdit   :: a -> Int -> IO a
        onEdgeDelete :: a -> (Int,Int) -> IO a
        onEdgeSelect :: a -> (Int,Int) -> IO a
        onGraphDraw  :: a -> Render ()
        
createDrawingArea :: GraphDrawable g => GraphEventManager g => TVar g -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode  -> TVar (Maybe (Double,Double)) -> ( Int -> IO ()) -> IO DrawingArea
createDrawingArea tGraph tSelected tDrag tPoint onEsc = do
        drawingArea <- drawingAreaNew
        drawingArea `widgetAddEvents` [KeyPressMask,KeyReleaseMask,ButtonMotionMask,ButtonPressMask,ButtonReleaseMask]
        drawingArea `onExpose` (\_ -> redrawArea drawingArea tGraph tSelected tDrag tPoint)
        drawingArea `on` motionNotifyEvent $ tryEvent $ manageDrawingMouseMotion tGraph tSelected tDrag tPoint
        drawingArea `on` buttonPressEvent $ tryEvent $ manageDrawingButtonPress tGraph tSelected tDrag tPoint
        --drawingArea `on` keyPressEvent $ tryEvent $ manageKeyPressEvent tGraph tSelected tDrag tPoint
        drawingArea `on` keyReleaseEvent $ tryEvent $ manageKeyPressEvent tGraph tSelected tDrag tPoint onEsc
        drawingArea `on` buttonReleaseEvent $ tryEvent $ manageDrawingButtonRelease tGraph tSelected tDrag tPoint
        set drawingArea [widgetCanFocus := True]
        return drawingArea
        
setupDrawingArea :: GraphDrawable g => GraphEventManager g => DrawingArea -> TVar g -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode  -> TVar (Maybe (Double,Double)) -> ( Int -> IO ()) -> IO DrawingArea
setupDrawingArea drawingArea tGraph tSelected tDrag tPoint onEsc = do
        drawingArea `widgetAddEvents` [KeyPressMask,KeyReleaseMask,ButtonMotionMask,ButtonPressMask,ButtonReleaseMask]
        drawingArea `onExpose` (\_ -> redrawArea drawingArea tGraph tSelected tDrag tPoint)
        drawingArea `on` motionNotifyEvent $ tryEvent $ manageDrawingMouseMotion tGraph tSelected tDrag tPoint
        drawingArea `on` buttonPressEvent $ tryEvent $ manageDrawingButtonPress tGraph tSelected tDrag tPoint
        --drawingArea `on` keyPressEvent $ tryEvent $ manageKeyPressEvent tGraph tSelected tDrag tPoint
        drawingArea `on` keyReleaseEvent $ tryEvent $ manageKeyPressEvent tGraph tSelected tDrag tPoint onEsc
        drawingArea `on` buttonReleaseEvent $ tryEvent $ manageDrawingButtonRelease tGraph tSelected tDrag tPoint
        set drawingArea [widgetCanFocus := True]
        return drawingArea
        
manageKeyPressEvent :: GraphDrawable g => GraphEventManager g =>  TVar g -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode -> TVar (Maybe (Double,Double))  -> ( Int -> IO ())  -> EventM EKey ()
manageKeyPressEvent tGraph tSelected tDrag tPoint onEsc = do
        modi <- eventModifier
        keyn <- eventKeyName
        liftIO $ do 
              print keyn
              when ("Delete" == keyn) $ do
                  graph <- readTVarIO tGraph
                  mSelected <- readTVarIO tSelected     
                  -- Ein Knoten wurde ausgewählt, und nun soll er gelöscht werden.
                  when (isJust mSelected && (isLeft . fromJust $ mSelected)) $ do
                          select <- return . fromLeft . fromJust  $ mSelected
                          ngraph <- onNodeDelete graph select 
                          atomically $ writeTVar tGraph ngraph
                          atomically $ writeTVar tSelected Nothing
               
              when ("Escape" == keyn) $ do onEsc 0
        return ()
        where fromLeft  (Left i)  = i
              fromLeft  (Right j) = error "Wufff!"
              fromRight (Left i)  = error "Wufff!"
              fromRight (Right j) = j
              fromJust (Just i) = i
              formJust (Nothing) = error "Wuff!"
              isRight (Right _) = True
              isRight (Left _)  = False
              isLeft = not. isRight
              isJust (Just _) = True
              isJust (Nothing) = False
        
manageDrawingMouseMotion :: GraphDrawable g => GraphEventManager g =>  TVar g -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode -> TVar (Maybe (Double,Double)) -> EventM EMotion ()
manageDrawingMouseMotion tGraph tSelected tDrag tPoint = do
        p     <- eventCoordinates
        mdf   <- eventModifierAll
        liftIO $ do
            drag <- atomically $ readTVar tDrag
            when (drag /= DragNone) $ do
               atomically $ writeTVar tPoint (Just p)
            return ()
        
manageDrawingButtonPress :: GraphDrawable g => GraphEventManager g =>  TVar g -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode -> TVar (Maybe (Double,Double)) -> EventM EButton ()
manageDrawingButtonPress tGraph tSelected tDrag tPoint = do
        p     <- eventCoordinates
        btn   <- eventButton
        click <- eventClick
        liftIO $ do 
                graph <- readTVarIO tGraph
                mSelected <- readTVarIO tSelected
                drag <- readTVarIO tDrag
                match <- return (filter (\(i,q) -> incircle p 40.0 q) (getNodePos graph))
                -- In diesem fall wird der Knoten selektiert, Eventuell startet hier ein Drag
                when (click == SingleClick && not (null match) && btn == LeftButton) $ do
                    i <- return . fst $ head match
                    ngraph <- onNodeSelect graph i
                    atomically $ writeTVar tGraph ngraph
                    atomically $ writeTVar tSelected (Just $ Left i)
                    atomically $ writeTVar tDrag DragNode
                    print "Press: SC L N -> Start Moving, Select Node"
                -- In diesem Fall soll ein Knoten bearbeitet werden,
                -- auf jeden fall ist der Fragvorgang vorbei
                when (click == DoubleClick && not (null match) && btn == LeftButton) $ do
                    i <- return . fst $ head match
                    ngraph <- onNodeEdit graph i
                    atomically $ writeTVar tGraph ngraph
                    atomically $ writeTVar tSelected (Just $ Left i)
                    atomically $ writeTVar tDrag DragNone
                    atomically $ writeTVar tPoint Nothing
                    print "Press: DC L N" 
                -- In diesem Fall soll ein neuer Knoten erstellt werden
                when (click == DoubleClick && null match && btn == LeftButton) $ do
                    ngraph <- onNodeCreate graph p
                    atomically $ writeTVar tGraph ngraph
                    atomically $ writeTVar tSelected Nothing
                    atomically $ writeTVar tDrag DragNone
                    atomically $ writeTVar tPoint Nothing
                    print "Press: DC L S" 
                -- In diesem Fall fängt ein Edge-Drag vorgang an.
                when (click == SingleClick && not (null match) && btn == RightButton) $ do
                    i <- return . fst $ head match
                    ngraph <- onNodeSelect graph i
                    atomically $ writeTVar tGraph ngraph
                    atomically $ writeTVar tSelected (Just $ Left i)
                    atomically $ writeTVar tDrag DragEdge
                    print "Press: SC R S" 
        return ()

incircle :: (Double,Double) -> Double -> (Double,Double) -> Bool
incircle (x,y)  r (xz,yz) = (x-xz)*(x-xz) + (y - yz)*(y - yz) < r*r

manageDrawingButtonRelease :: GraphDrawable g => GraphEventManager g =>  TVar g  -> TVar (Maybe (Either Int (Int,Int))) -> TVar DragMode -> TVar (Maybe (Double,Double)) -> EventM EButton ()
manageDrawingButtonRelease tGraph tSelected tDrag tPoint = do
        p     <- eventCoordinates
        btn   <- eventButton
        click <- eventClick
        liftIO $ do 
                graph <- readTVarIO tGraph
                mSelected <- readTVarIO tSelected
                drag <- readTVarIO tDrag
                match <- return (filter (\(i,q) -> incircle p 40.0 q) (getNodePos graph))
                -- In diesem Fall wird ein Drag beendet
                when (click == ReleaseClick && (null match) && btn == LeftButton && drag == DragNode) $ do
                    case mSelected of
                        Nothing -> return ()
                        Just (Left i) -> do
                            ngraph <- onNodeMove graph i p
                            atomically $ writeTVar tGraph ngraph
                        Just (Right (i,j)) -> return ()
                    atomically $ writeTVar tSelected Nothing
                    atomically $ writeTVar tDrag DragNone
                    atomically $ writeTVar tPoint Nothing
                    print "Release: DC L: End a NodeDrag"
                -- In diesem Fall wird ein Drag beendet
                when (click == ReleaseClick && not (null match) && btn == RightButton && drag == DragEdge) $ do
                    j <- return . fst $ head match
                    case mSelected of
                        Nothing -> return ()
                        Just (Left i) -> do
                            ngraph <- onEdgeCreate graph i j
                            atomically $ writeTVar tGraph ngraph
                        Just (Right (_,_)) -> return ()
                    atomically $ writeTVar tSelected Nothing
                    atomically $ writeTVar tDrag DragNone
                    atomically $ writeTVar tPoint Nothing
                    print "Release: DC L: End a EdgeDrag"
                when (click == ReleaseClick && btn == LeftButton && drag == DragNode) $ do
                    atomically $ writeTVar tSelected Nothing
                    atomically $ writeTVar tDrag DragNone
                    print "Release: DC L:"  
        return ()

  
redrawArea :: GraphDrawable g => GraphEventManager g => DrawingArea -> TVar g -> TVar (Maybe (Either Int (Int,Int)))  -> TVar DragMode -> TVar (Maybe (Double,Double)) -> IO Bool
redrawArea area tGraph tSelection tDrag tPoint = do
        win <- widgetGetDrawWindow area
        mSelection <- readTVarIO tSelection
        graph <- readTVarIO tGraph
        drag <- readTVarIO tDrag
        point <- readTVarIO tPoint
        renderWithDrawable win $ do
                drawGraph graph mSelection
                closePath
                drawDrag point graph drag mSelection
                return True
                
                
drawGraph :: GraphDrawable g => GraphEventManager g => g -> Maybe (Either Int (Int,Int)) ->  Render Bool
drawGraph graph mSelection = do 
        mapM_ (\(i,(dx,dy)) -> do { (if mSelection == Just (Left i) then setSourceRGB 1 1 1 else setSourceRGB 0 0 0);  arc dx dy nodeRadius 0 (2*pi); stroke; setSourceRGB 0 0 0 }) (getNodePos graph)
        mapM_ (func) (getEdges graph)
        onGraphDraw graph
        return True
        where func = \(i,j) -> renderConn (getPos graph i) (getPos graph j) True
        
getPos :: GraphDrawable a => a -> Int -> (Double,Double)
getPos graph i = foldl (\s (j,n) -> if i == j then n else s ) (0,0) (getNodePos graph)

drawDrag :: GraphDrawable g => Maybe (Double,Double) -> g -> DragMode -> Maybe (Either Int (Int,Int)) -> Render ()
drawDrag (Just (x,y)) graph DragNode (Just (Left _)) = do
               setSourceRGB 0.8 0.8 0.8
               arc x y nodeRadius 0 (2*pi)
               stroke
               setSourceRGB 0 0 0
drawDrag (Just (x,y)) graph DragEdge (Just (Left v)) = do
               setSourceRGB 0.6 0.6 0.6
               ((x1,y1),(x2,y2),(z1,z2),(u1,u2)) <- calcRenderPos (getPos graph v) (x,y) False
               moveTo x1 y1
               lineTo x2 y2
               lineTo z1 z2
               moveTo x2 y2
               lineTo u1 u2
               stroke
               closePath
               stroke
               setSourceRGB 0 0 0
drawDrag _ _ _ _ = return ()
               
calcRenderPos :: (Double,Double) -> (Double,Double) -> Bool -> Render ((Double,Double),(Double,Double),(Double,Double),(Double,Double))
calcRenderPos (x,y) (z,w) b = do
          (x1,y1) <- return ((x + sintan (abs(x-z)) (abs(y-w))*signum(z-x)),(y + costan (abs(x-z)) (abs(y-w))*signum(w-y)))
          (x2,y2) <- if b then return ((z + sintan (abs(x-z)) (abs(y-w))*signum(x-z)),(w + costan (abs(x-z)) (abs(y-w))*signum(y-w))) else return (z,w)
          (w,h)   <- return $ (abs(x2-x1),abs(y1-y2))
          l       <- return $ sqrt $ ( (w*w)+(h*h) )
          k       <- return $ 20.0
          beta    <- return $ atan (w/h)
          gamma   <- return $ 30 * pi / 180
          (z1,z2) <- return $ (if x1 - x2 > 0 then x2+sin(beta-gamma)*k else x2-sin(beta-gamma)*k,if y1-y2 >0 then y2+cos(beta-gamma)*k else y2-cos(beta-gamma)*k)
          (u1,u2) <- return $ (if x1 - x2 > 0 then x2+sin(beta+gamma)*k else x2-sin(beta+gamma)*k,if y1-y2 >0 then y2+cos(beta+gamma)*k else y2-cos(beta+gamma)*k)
          return ((x1,y1),(x2,y2),(z1,z2),(u1,u2))
          where sintan,costan :: Double -> Double -> Double
                sintan a b = (sin . atan) (a/b) * 40.0
                costan a b = (cos . atan) (a/b) * 40.0
          
-- | renderConn Zeichnet eine Kante des Graphen
renderConn :: (Double,Double) -> (Double,Double) -> Bool -> Render ()
renderConn (x,y) (z,w) b = do
          ((x1,y1),(x2,y2),(z1,z2),(u1,u2)) <- calcRenderPos (x,y) (z,w) b
          setFontSize 15
          moveTo (x1+7+(x2-x1)/2) (y1+(y2-y1)/2)
          showText "Text"
          moveTo x1 y1
          lineTo x2 y2
          lineTo z1 z2
          moveTo x2 y2
          lineTo u1 u2
          stroke
          closePath
          where sintan,costan :: Double -> Double -> Double
                sintan a b = (sin . atan) (a/b) * 40.0
                costan a b = (cos . atan) (a/b) * 40.0
        