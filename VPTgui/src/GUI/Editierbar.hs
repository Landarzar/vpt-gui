{-# LANGUAGE FlexibleInstances#-}
module GUI.Editierbar where

class (Show a) => Editierbar a where
      onEdit :: String -> Maybe a
